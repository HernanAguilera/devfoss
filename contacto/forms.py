# -*- coding: utf-8 -*-
from django import forms

class contactForm(forms.Form):
    remitente = forms.CharField(label='Usuario',max_length=64, required=True)
    correo = forms.CharField(label='Contraseña',max_length=256, widget=forms.PasswordInput, required=True)
    asunto = forms.CharField(label='Asunto', max_length=128, required=True)
    mensaje = forms.CharField(label='Mensaje', widget=forms.Textarea, required=True)
