from django.shortcuts import render, render_to_response
from django.http import HttpResponse
from contacto.forms import contactForm

def index(request):
    form = contactForm()
    return render(request, 'contacto/index.html', {'form':form})
