from django.shortcuts import render, render_to_response
from django.http import HttpResponse
from django.template import RequestContext

def home(request):
    return render_to_response('home.html', {'user': request.user})
