from django.conf.urls import patterns, include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', 'devfoss.views.home', name='home'),

	url(r'^blog/', include('blog.urls')),
	url(r'^contacto/', include('contacto.urls')),
	url(r'^contenido/', include('contenidos.urls')),

	url(r'^autenticacion/', include('autenticacion.urls')),

    url(r'^admin/', include(admin.site.urls)),
)


urlpatterns += staticfiles_urlpatterns()
