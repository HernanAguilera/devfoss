# -*- coding: utf-8 -*-
from django import forms

class loginForm(forms.Form):
    username = forms.CharField(label='Usuario',max_length=64, required=True)
    password = forms.CharField(label='Contraseña',max_length=256, widget=forms.PasswordInput, required=True)
