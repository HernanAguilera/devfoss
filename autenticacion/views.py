# -*- coding: utf-8 -*-
from django.shortcuts import render, render_to_response, redirect
from django.http import HttpResponse
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from autenticacion.forms import loginForm

def login(request):
    if request.user.is_authenticated():
        return redirect('home')
    
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                auth_login(request, user)
                return redirect('home')
            else:
                form = loginForm()
                return render(request, 'autenticacion/login.html', {'form':form, 'mensaje': 'este usuario esta inactivo'})
        else:
            form = loginForm()
            return render(request, 'autenticacion/login.html', {'form':form, 'mensaje': 'usuario o contraseña incorrectos'})
    else:
        form = loginForm()
        return render(request, 'autenticacion/login.html', {'form':form})

def logout(request):
    if not request.user.is_authenticated():
        return redirect('home')
    auth_logout(request)
    return redirect('home')
