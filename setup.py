from setuptools import setup

setup(name='DevFOSS',
    version='1.0',
    description='Aplicacion web',
    author='Hernan Aguielera',
    author_email='hernan@devfoss.org',
    url='http://www.python.org/sigs/distutils-sig/',
    install_requires=['Django>=1.6', 'django-embed-video>=0.11', 'django-markdown>=0.6'],
)
