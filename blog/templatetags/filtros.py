from django import template
import logging

logger = logging.getLogger('devfoss')
register = template.Library()

@register.filter(name='add_css')
def add_css(field, css):
   return field.as_widget(attrs={"class":css})


@register.filter(name='bootstrap_class')
def boostrap_class(field):
   inputs = ('TextInput', 'NumberInput', 'EmailInput', 'URLInput', 'PasswordInput', 'DateInput', 'DateTimeInput', 'TimeInput', 'Textarea')
   if get_type(field) in inputs:
      return field.as_widget(attrs={"class":"form-control"})
   return field

@register.filter(name='get_atribute')
def get_atribute(field, atribute):
   return getattr(field, atribute)

def get_type(field):
   return field.field.widget.__class__.__name__