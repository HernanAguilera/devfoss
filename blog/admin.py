from django.contrib import admin
from django_markdown.admin import MarkdownModelAdmin
from blog.models import Articulo, Comentario, Pagina, Puntuacion

admin.site.register(Pagina, MarkdownModelAdmin)
admin.site.register(Articulo, MarkdownModelAdmin)
admin.site.register(Comentario, MarkdownModelAdmin)
admin.site.register(Puntuacion, MarkdownModelAdmin)
