from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^admin/paginas/$', views.PaginaList.as_view(), name='list_pagina'),
    url(r'^admin/paginas/create/$', views.PaginaCreate.as_view(), name='create_pagina'),
    url(r'^admin/paginas/(?P<pk>\d+)/$', views.PaginaShow.as_view(), name='show_pagina'),
    url(r'^admin/paginas/(?P<pk>\d+)/edit/$', views.PaginaEdit.as_view(), name='edit_pagina'),
    url(r'^admin/paginas/(?P<pk>\d+)/delete/$', views.PaginaDelete.as_view(), name='delete_pagina'),
    
    url(r'^admin/articulos/$', views.ArticuloList.as_view(), name='list_articulo'),
    url(r'^admin/articulos/create/$', views.ArticuloCreate.as_view(), name='create_articulo'),
    url(r'^admin/articulos/(?P<pk>\d+)/$', views.ArticuloShow.as_view(), name='show_articulo'),
    url(r'^admin/articulos/(?P<pk>\d+)/edit/$', views.ArticuloEdit.as_view(), name='edit_articulo'),
    url(r'^admin/articulos/(?P<pk>\d+)/delete/$', views.ArticuloDelete.as_view(), name='delete_articulo'),
    
    url(r'^admin/comentarios/$', views.ComentarioList.as_view(), name='list_comentario'),
    url(r'^admin/comentarios/create/$', views.ComentarioCreate.as_view(), name='create_comentario'),
    url(r'^admin/comentarios/(?P<pk>\d+)/$', views.ComentarioShow.as_view(), name='show_comentario'),
    url(r'^admin/comentarios/(?P<pk>\d+)/edit/$', views.ComentarioEdit.as_view(), name='edit_comentario'),
    url(r'^admin/comentarios/(?P<pk>\d+)/delete/$', views.ComentarioDelete.as_view(), name='delete_comentario'),
    
    #public
    url(r'^$', views.home, name='blog_home'),
    url(r'^(?P<articulo_id>\d+)/(?P<slug>\S+)/$', views.ArticuloDetail.as_view(), name='detail_articulo'),
    url(r'^(?P<slug>\S+)/$', views.PaginaDetail.as_view(), name='detail_pagina'),
    #url(r'^articulos/$', views, name='index'),
    #url(r'^articulos/create/$', views.create, name='articulos_create'),
    #url(r'^articulos/store/$', views.store, name='articulos_store'),
    #url(r'^articulos/(?P<articulo_id>\d+)/$', views.show, name='articulos_show'),
    #url(r'^articulos/(?P<articulo_id>\d+)/edit/$', views.edit, name='articulos_edit'),
    #url(r'^articulos/(?P<articulo_id>\d+)/update/$', views.update, name='articulos_update'),
    #url(r'^articulos/(?P<articulo_id>\d+)/delete/$', views.delete, name='articulos_delete'),
]