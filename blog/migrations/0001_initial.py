# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('taxonomia', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Nodo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('titulo', models.CharField(max_length=256)),
                ('cuerpo', models.TextField(null=True)),
                ('fecha_creacion', models.DateTimeField(auto_now_add=True)),
                ('fecha_actualizacion', models.DateTimeField(auto_now=True)),
                ('publicado', models.BooleanField(default=True)),
            ],
            options={
                'ordering': ['-fecha_creacion'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Contenido',
            fields=[
                ('nodo_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='blog.Nodo')),
                ('slug', models.SlugField(max_length=256, blank=True)),
                ('en_homepage', models.BooleanField(default=True)),
                ('destacado', models.BooleanField(default=False)),
                ('permitir_comentarios', models.BooleanField(default=True)),
            ],
            options={
            },
            bases=('blog.nodo',),
        ),
        migrations.CreateModel(
            name='Articulo',
            fields=[
                ('contenido_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='blog.Contenido')),
            ],
            options={
                'verbose_name_plural': 'articulos',
            },
            bases=('blog.contenido',),
        ),
        migrations.CreateModel(
            name='Comentario',
            fields=[
                ('nodo_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='blog.Nodo')),
            ],
            options={
                'verbose_name_plural': 'comentarios',
            },
            bases=('blog.nodo',),
        ),
        migrations.CreateModel(
            name='Pagina',
            fields=[
                ('contenido_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='blog.Contenido')),
            ],
            options={
                'verbose_name_plural': 'paginas',
            },
            bases=('blog.contenido',),
        ),
        migrations.CreateModel(
            name='Puntuacion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('valor', models.CharField(default=b'0', max_length=2, choices=[(b'1', b'positiva'), (b'0', b'neutral'), (b'-1', b'negativa')])),
                ('fecha_creacion', models.DateTimeField(auto_now_add=True)),
                ('fecha_actualizacion', models.DateTimeField(auto_now=True)),
                ('autor', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
                ('nodo', models.ForeignKey(to='blog.Nodo')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Referencia',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('titulo', models.CharField(max_length=128)),
                ('autor', models.CharField(max_length=128)),
                ('url', models.URLField(max_length=256, null=True)),
                ('preview', models.ImageField(max_length=512, null=True, upload_to=b'')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='nodo',
            name='autor',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='contenido',
            name='referencia',
            field=models.ManyToManyField(to='blog.Referencia', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='contenido',
            name='tags',
            field=models.ManyToManyField(to='taxonomia.Tag', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='comentario',
            name='nodo',
            field=models.ForeignKey(related_name=b'comentarios', to='blog.Nodo'),
            preserve_default=True,
        ),
    ]
