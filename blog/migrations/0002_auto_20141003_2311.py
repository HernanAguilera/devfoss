# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('taxonomia', '0002_categoria'),
        ('blog', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='contenido',
            name='categoria',
            field=models.ForeignKey(blank=True, to='taxonomia.Categoria', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='contenido',
            name='referencia',
            field=models.ManyToManyField(to=b'blog.Referencia', null=True, blank=True),
        ),
    ]
