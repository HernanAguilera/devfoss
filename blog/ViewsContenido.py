from django.views.generic import ListView, DetailView, View
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.shortcuts import render
from blog.forms import ComentarioForm
from blog.models import Contenido
import logging
logger = logging.getLogger('devfoss')

'''
clases base para las vistas
'''
class ContenidoList(ListView):
    contenido_nombre = 'contenido'
    template_name = 'blog/contenidos/index.html'
    context_object_name = 'contenidos'
    
    def get_context_data(self, **kwargs):
        context = super(ContenidoList, self).get_context_data(**kwargs)
        context['contenido_nombre'] = self.contenido_nombre
        return context
    
class HijoContenidoList(ContenidoList):
    template_name = 'blog/contenidos/hijos/index.html'
    
    def get_context_data(self, **kwargs):
        context = super(HijoContenidoList, self).get_context_data(**kwargs)
        context['base_nombre'] = self.base_nombre
        return context

class ContenidoShow(DetailView):
    contenido_nombre = 'contenido'
    template_name = 'blog/contenidos/show.html'
    context_object_name = 'contenido'
    
    def get_context_data(self, **kwargs):
        context = super(ContenidoShow, self).get_context_data(**kwargs)
        context['contenido_nombre'] = self.contenido_nombre
        return context
    
class HijoContenidoShow(ContenidoShow):
    template_name = 'blog/contenidos/hijos/show.html'

class ContenidoCreate(CreateView):
    contenido_nombre = 'contenido'
    template_name = 'blog/contenidos/create.html'
    
    def get_context_data(self, **kwargs):
        context = super(ContenidoCreate, self).get_context_data(**kwargs)
        context['contenido_nombre'] = self.contenido_nombre
        return context
    
    def form_valid(self, form):
        form.instance.autor = self.request.user
        return super(ContenidoCreate, self).form_valid(form)
    
class HijoContenidoCreate(ContenidoCreate):
    template_name = 'blog/contenidos/hijos/create.html'
    
    def get(self, request, *args, **kwargs):
        try:
            self.base_obj = self.base_model.objects.get(pk=kwargs['base_id'])
        except:
            self.base_obj = None
        return super(HijoContenidoCreate, self).get(self, request, *args, **kwargs)
    
    def get_context_data(self, **kwargs):
        context = super(HijoContenidoCreate, self).get_context_data(**kwargs)
        context['base'] = self.base_obj
        return context
    
    def post(self, request, *args, **kwargs):
        try:
            self.base_obj = self.base_model.objects.get(pk=kwargs['base_id'])
        except:
            self.base_obj = None
        return super(HijoContenidoCreate, self).post(self, request, *args, **kwargs)
    
    def form_valid(self, form):
        setattr(form.instance, self.base_nombre, self.base_obj)
        return super(HijoContenidoCreate, self).form_valid(form)
       
class ContenidoEdit(UpdateView):
    contenido_nombre = 'contenido'
    template_name = 'blog/contenidos/create.html'
    context_object_name = 'contenido'
    
    def get_context_data(self, **kwargs):
        context = super(ContenidoEdit, self).get_context_data(**kwargs)
        context['contenido_nombre'] = self.contenido_nombre
        return context
    
class HijoContenidoEdit(ContenidoEdit):
    template_name = 'blog/contenidos/hijos/create.html'
    
    def get(self, request, *args, **kwargs):
        try:
            self.base_obj = self.base_model.objects.get(pk=kwargs['base_id'])
        except:
            self.base_obj = None
        return super(HijoContenidoEdit, self).get(self, request, *args, **kwargs)
    
    def get_context_data(self, **kwargs):
        context = super(HijoContenidoEdit, self).get_context_data(**kwargs)
        context['base'] = self.base_obj
        return context

class ContenidoDelete(DeleteView):
    contenido_nombre = 'contenido'
    template_name = 'blog/contenidos/delete_confirm.html'
    context_object_name = 'contenido'
    
    def get_context_data(self, **kwargs):
        context = super(ContenidoDelete, self).get_context_data(**kwargs)
        context['contenido_nombre'] = self.contenido_nombre
        return context
    
class HijoContenidoDelete(ContenidoDelete):
    template_name = 'blog/contenidos/hijos/delete_confirm.html'

class ContenidoDetail(View):
    contenido_nombre = 'contenido'
    template_name = 'blog/contenidos/detail.html'
    context_object_name = 'contenido'
    model = Contenido
    
    def get(self, request, *args, **kwargs):
        object_display = self.get_object_display(*args, **kwargs)
        form = ComentarioForm()
        return render(request, self.template_name, {self.context_object_name : object_display, 'form': form, 'contenido_nombre': self.contenido_nombre})
            
    def post(self, request, *args, **kwargs):
        object_display = self.get_object_display(*args, **kwargs)
        form = ComentarioForm(request.POST)
        if request.POST['cuerpo']:
            form.instance.titulo = request.POST['cuerpo'][:33]
            if len(request.POST['cuerpo'])>32:
                form.instance.titulo += '...'
        form.instance.nodo = object_display
        form.instance.autor = request.user
        if form.is_valid():
            form.save()
            form = ComentarioForm()
            return render(request, self.template_name, {self.context_object_name : object_display, 'form': form, 'mensaje': 'tu comentario fue publicado'})
        else:
            return render(request, self.template_name, {self.context_object_name : object_display, 'form': form, 'mensaje': 'hubo problemas al procesar tu comentario'})
    
    def get_object_display(self, *args, **kwargs):
        return self.model.objects.get(slug=kwargs['slug'])
    
class HijoContenidoDetail(ContenidoDetail):
    template_name = 'blog/contenidos/hijos/detail.html'
