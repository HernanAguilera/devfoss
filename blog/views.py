from django.shortcuts import render, render_to_response, get_object_or_404, redirect
from django.http import HttpResponse
from django.core.urlresolvers import reverse_lazy
from blog.models import Pagina, Articulo, Comentario
from blog.ViewsContenido import ContenidoCreate, ContenidoList, ContenidoShow, ContenidoEdit, ContenidoDelete, ContenidoDetail

titulo = 'DevFOSS'

'''
HOMEPAGE BLOG
'''
def home(request):
    articulos = Articulo.objects.all()
    return render_to_response('blog/home.html', {'titulo':'Blog | ' + titulo, 'articulos': articulos})

'''
PAGINAS
'''
class PaginaList(ContenidoList):
    model = Pagina
    contenido_nombre = 'pagina'
    
class PaginaCreate(ContenidoCreate):
    model = Pagina
    fields = ['titulo', 'cuerpo', 'publicado', 'en_homepage', 'destacado', 'tags']
    contenido_nombre = 'pagina'
    
class PaginaShow(ContenidoShow):
    model = Pagina
    contenido_nombre = 'pagina'
    
class PaginaEdit(ContenidoEdit):
    model = Pagina
    contenido_nombre = 'pagina'
    
class PaginaDelete(ContenidoDelete):
    model = Pagina
    contenido_nombre = 'pagina'
    success_url = reverse_lazy('list_pagina')

''' detail_pagina sera publica '''
class PaginaDetail(ContenidoDetail):
    model = Pagina
    contenido_nombre = 'pagina'

'''
ARTICULOS
'''
class ArticuloList(ContenidoList):
    model = Articulo
    contenido_nombre = 'articulo'
    
class ArticuloCreate(ContenidoCreate):
    model = Articulo
    fields = ['titulo', 'cuerpo', 'publicado', 'en_homepage', 'destacado', 'tags']
    contenido_nombre = 'articulo'
    
class ArticuloShow(ContenidoShow):
    model = Articulo
    template_name = 'articulos/show.html'
    context_object_name = 'articulo'
    
class ArticuloEdit(ContenidoEdit):
    model = Articulo
    contenido_nombre = 'articulo'
    
class ArticuloDelete(ContenidoDelete):
    model = Articulo
    contenido_nombre = 'articulo'
    success_url = reverse_lazy('list_articulo')

''' detail_articulo sera publico '''
class ArticuloDetail(ContenidoDetail):
    model = Articulo
    contenido_nombre = 'articulo'

'''
COMENTARIOS
'''
class ComentarioList(ContenidoList):
    model = Comentario
    contenido_nombre = 'comentario'
    
class ComentarioCreate(ContenidoCreate):
    model = Comentario
    contenido_nombre = 'comentario'
    
class ComentarioShow(ContenidoShow):
    model = Comentario
    contenido_nombre = 'comentario'
    
class ComentarioEdit(ContenidoEdit):
    model = Comentario
    contenido_nombre = 'comentario'
    
class ComentarioDelete(ContenidoDelete):
    model = Comentario
    contenido_nombre = 'comentario'
    success_url = reverse_lazy('list_comentario')
