from django.db import models
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django.template.defaultfilters import slugify
from taxonomia.models import Tag, Categoria

'''
Clase base de la cual herendan todo lo que vaya a ser publicado
'''
class Nodo(models.Model):
    titulo = models.CharField(max_length=256)
    cuerpo = models.TextField(null=True)
    
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_actualizacion = models.DateTimeField(auto_now=True)
    
    publicado = models.BooleanField(default=True)
    
    autor = models.ForeignKey(User)
    
    class Meta:
        ordering = ['-fecha_creacion']

    def __unicode__(self):
        return self.titulo

'''
Puntucion sobre los contenidos
'''
class Puntuacion(models.Model):
    opciones = [
        ('1', 'positiva'),
        ('0', 'neutral'),
        ('-1', 'negativa'),
    ]
    autor = models.ForeignKey(User)
    nodo = models.ForeignKey(Nodo)
    valor = models.CharField(max_length=2, choices=opciones, default='0')
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_actualizacion = models.DateTimeField(auto_now=True)
    
    def __unicode__(self):
        return 'puntuacion ' + self.valor + ' en ' + self.nodo.titulo


'''
Referencias del contenido
'''
class Referencia(models.Model):
    titulo = models.CharField(max_length=128)
    autor = models.CharField(max_length=128)
    url = models.URLField(max_length=256, null=True)
    preview = models.ImageField(max_length=512, null=True)
    
    def __unicode__(self):
        return self.titulo

'''
Clase de la cual heredan todos los cotenidos
'''
class Contenido(Nodo):
    slug = models.SlugField(max_length=256, blank=True)
    
    en_homepage = models.BooleanField(default=True)
    destacado = models.BooleanField(default=False)
    permitir_comentarios = models.BooleanField(default=True)
    
    tags = models.ManyToManyField(Tag, blank=True)
    categoria = models.ForeignKey(Categoria, null=True, blank=True)
    referencia = models.ManyToManyField(Referencia, null=True, blank=True)
    
    absolute_url = 'show_contenido'
    
    def save(self, *args, **kwargs):
        self.slug = slugify(self.titulo)
        return super(Contenido, self).save(*args, **kwargs)
    
    def get_absolute_url(self):
        return reverse(self.absolute_url, kwargs={'pk': self.pk})

'''
Clase para comentarios
'''
class Comentario(Nodo):
    nodo = models.ForeignKey(Nodo, related_name='comentarios')
    
    class Meta:
        verbose_name_plural = 'comentarios'
    
    def get_absolute_url(self):
        return reverse('show_comentario', kwargs={'pk': self.pk})
    
'''
Contenido dinamico
'''
class Articulo(Contenido):
    absolute_url = 'show_articulo'
    class Meta:
        verbose_name_plural = 'articulos'

'''
Contenido estatico
'''
class Pagina(Contenido):
    absolute_url = 'show_pagina'
    class Meta:
        verbose_name_plural = 'paginas'