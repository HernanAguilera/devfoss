from embed_video.backends import VideoBackend
import re

class YoutubePlayListBackend(VideoBackend):
    #http://www.youtube.com/playlist?list=PLos-7g0HK0n4OjBt5j1pHqwZY68P7HaoD
    re_detect = re.compile(r'(http:)?//(www\.)?youtube\.com/playlist\?list=[\w\-]+')
    #re_detect = re.compile(r'http://myvideo\.com/[0-9]+')
    re_code = re.compile(r'(http:)?//(www\.)?youtube\.com/playlist\?list=(?P<code>[\w\-]+)')
    #re_code = re.compile(r'http://myvideo\.com/(?P<code>[0-9]+)')

    allow_https = False
    pattern_url = '//www.youtube.com/embed/videoseries?list={code}'
    #pattern_url = '{protocol}://play.myvideo.com/c/{code}/'
    pattern_thumbnail_url = '{protocol}://img.youtube.com/vi/{code}/hqdefault.jpg'
    
    #template_name = 'embed_video/custombackend_embed_code.html'
