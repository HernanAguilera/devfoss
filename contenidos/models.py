from django.db import models
from django.core.urlresolvers import reverse
from blog.models import Contenido

class PlayList(Contenido):
    direccion = models.URLField(max_length=256, null=True, blank=True)
    thumb = models.FileField(max_length=512, null=True, blank=True)
    
class Video(Contenido):
    direccion = models.URLField(max_length=256)
    playlist = models.ForeignKey(PlayList, null=True, blank=True, related_name='videos')
    absolute_url = 'show_video'

class Descarga(Contenido):
    url = models.URLField(max_length=256)
    absolute_url = 'show_descarga'

class Encuesta(Contenido):
    absolute_url = 'show_encuesta'
    
class Opcion(Contenido):
    encuesta = models.ForeignKey(Encuesta)

class Libro(Contenido):
    absolute_url = 'show_videotutorial'
    
class PaginaLibro(Contenido):
    libro = models.ForeignKey(Libro)