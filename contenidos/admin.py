from django.contrib import admin
from django_markdown.admin import MarkdownModelAdmin
from contenidos.models import PlayList, Video, Descarga, Encuesta, Opcion

admin.site.register(PlayList, MarkdownModelAdmin)
admin.site.register(Video, MarkdownModelAdmin)
admin.site.register(Encuesta, MarkdownModelAdmin)
admin.site.register(Opcion, MarkdownModelAdmin)
admin.site.register(Descarga, MarkdownModelAdmin)
