from django.shortcuts import render, render_to_response
from django.http import HttpResponse
from django.core.urlresolvers import reverse_lazy
from contenidos.models import *
from blog.ViewsContenido import *
import logging
logger = logging.getLogger(__name__)

def admin(request):
    return render_to_response('contenidos/admin.html')

def playlist_home(request):
    playlists = PlayList.objects.all()
    return render_to_response('contenidos/playlists.html', {'titulo':'PlayLists | DevFOSS', 'playlists': playlists})

def encuesta_home(request):
    encuestas = Encuesta.objects.all()
    return render_to_response('contenidos/playlists.html', {'titulo':'Encuestas | DevFOSS', 'encuestas': encuestas})

class PlayListList(ContenidoList):
    model = PlayList
    template_name = 'contenidos/playlists/index.html'
    context_object_name = 'playlists'
    
class PlayListCreate(ContenidoCreate):
    model = PlayList
    fields = ['titulo', 'cuerpo', 'direccion', 'publicado', 'en_homepage', 'destacado', 'tags']
    template_name = 'contenidos/playlists/create.html'
    
class PlayListShow(ContenidoShow):
    model = PlayList
    template_name = 'contenidos/playlists/show.html'
    context_object_name = 'playlist'
    
class PlayListEdit(ContenidoEdit):
    model = PlayList
    template_name = 'contenidos/playlists/create.html'
    context_object_name = 'playlist'
    
class PlayListDelete(ContenidoDelete):
    model = PlayList
    template_name = 'contenidos/playlists/delete_confirm.html'
    context_object_name = 'playlist'
    success_url = reverse_lazy('list_playlist')

''' detail_pagina sera publica '''
class PlayListDetail(ContenidoDetail):
    model = PlayList
    template_name = 'contenidos/playlists/detail.html'
    context_object_name = 'playlist'
    

'''
Video
'''
class VideoList(HijoContenidoList):
    model = Video
    contenido_nombre = 'video'
    base_nombre = 'playlist'
    
class VideoCreate(HijoContenidoCreate):
    model = Video
    contenido_nombre = 'video'
    base_model = PlayList
    base_nombre = 'playlist'
    fields = [
         'titulo'
        ,'cuerpo'
        ,'direccion'
        ,'publicado'
        ,'en_homepage'
        ,'destacado'
        ,'tags'
    ]
    
class VideoShow(HijoContenidoShow):
    model = Video
    contenido_nombre = 'video'
    
class VideoEdit(HijoContenidoEdit):
    model = Video
    contenido_nombre = 'video'
    base_model = PlayList
    base_nombre = 'playlist'
    fields = [
         'titulo'
        ,'cuerpo'
        ,'playlist'
        ,'direccion'
        ,'publicado'
        ,'en_homepage'
        ,'destacado'
        ,'tags'
    ]
    
class VideoDelete(HijoContenidoDelete):
    model = Video
    contenido_nombre = 'video'
    success_url = reverse_lazy('list_video')

''' detail_pagina sera publica '''
class VideoDetail(ContenidoDetail):
    model = Video
    template_name = 'contenidos/videos/detail.html'
    context_object_name = 'video'
    

'''
ENCUESTAS
'''
class EncuestaList(ContenidoList):
    model = Encuesta
    contenido_nombre = 'encuesta'
    
class EncuestaCreate(ContenidoCreate):
    model = Encuesta
    contenido_nombre = 'encuesta'
    fields = [
         'titulo'
        ,'cuerpo'
        ,'publicado'
        ,'en_homepage'
        ,'destacado'
        ,'tags'
        ,'categoria'
    ]
    
class EncuestaShow(ContenidoShow):
    model = Encuesta
    contenido_nombre = 'encuesta'
    
class EncuestaEdit(ContenidoEdit):
    model = Encuesta
    contenido_nombre = 'encuesta'
    
class EncuestaDelete(ContenidoDelete):
    model = Encuesta
    contenido_nombre = 'encuesta'
    success_url = reverse_lazy('list_encuesta')

''' detail_encuesta sera publica '''
class EncuestaDetail(ContenidoDetail):
    model = Encuesta
    contenido_nombre = 'encuesta'
    
    
    '''
OPCIONES
'''
class OpcionList(HijoContenidoList):
    model = Opcion
    contenido_nombre = 'opcion'
    base_model = Encuesta
    base_nombre = 'encuesta'
    
class OpcionCreate(HijoContenidoCreate):
    model = Opcion
    contenido_nombre = 'opcion'
    base_model = Encuesta
    base_nombre = 'encuesta'
    fields = [
         'titulo'
        ,'cuerpo'
        ,'encuesta'
        ,'publicado'
        ,'en_homepage'
        ,'destacado'
        ,'tags'
        ,'categoria'
    ]
    
class OpcionShow(HijoContenidoShow):
    model = Opcion
    contenido_nombre = 'opcion'
    
class OpcionEdit(HijoContenidoEdit):
    model = Opcion
    contenido_nombre = 'opcion'
    
class OpcionDelete(HijoContenidoDelete):
    model = Opcion
    contenido_nombre = 'opcion'
    success_url = reverse_lazy('list_opcion')
    

'''
OPCIONES
'''
class DescargaList(ContenidoList):
    model = Descarga
    contenido_nombre = 'descarga'
    
class DescargaCreate(ContenidoCreate):
    model = Descarga
    contenido_nombre = 'descarga'
    fields = [
         'titulo'
        ,'cuerpo'
        ,'publicado'
        ,'en_homepage'
        ,'destacado'
        ,'tags'
        ,'categoria'
    ]
    
class DescargaShow(ContenidoShow):
    model = Descarga
    contenido_nombre = 'descarga'
    
class DescargaEdit(ContenidoEdit):
    model = Descarga
    contenido_nombre = 'descarga'
    
class DescargaDelete(ContenidoDelete):
    model = Descarga
    contenido_nombre = 'descarga'
    success_url = reverse_lazy('list_descarga')