# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('contenidos', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='playlist',
            name='thumb',
            field=models.FileField(max_length=512, null=True, upload_to=b'', blank=True),
        ),
        migrations.AlterField(
            model_name='video',
            name='playlist',
            field=models.ForeignKey(related_name=b'videos', to='contenidos.PlayList', null=True),
        ),
    ]
