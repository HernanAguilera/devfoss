# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Descarga',
            fields=[
                ('contenido_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='blog.Contenido')),
                ('url', models.URLField(max_length=256)),
            ],
            options={
            },
            bases=('blog.contenido',),
        ),
        migrations.CreateModel(
            name='Encuesta',
            fields=[
                ('contenido_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='blog.Contenido')),
            ],
            options={
            },
            bases=('blog.contenido',),
        ),
        migrations.CreateModel(
            name='Libro',
            fields=[
                ('contenido_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='blog.Contenido')),
            ],
            options={
            },
            bases=('blog.contenido',),
        ),
        migrations.CreateModel(
            name='Opcion',
            fields=[
                ('contenido_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='blog.Contenido')),
                ('encuesta', models.ForeignKey(to='contenidos.Encuesta')),
            ],
            options={
            },
            bases=('blog.contenido',),
        ),
        migrations.CreateModel(
            name='PaginaLibro',
            fields=[
                ('contenido_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='blog.Contenido')),
                ('libro', models.ForeignKey(to='contenidos.Libro')),
            ],
            options={
            },
            bases=('blog.contenido',),
        ),
        migrations.CreateModel(
            name='PlayList',
            fields=[
                ('contenido_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='blog.Contenido')),
                ('direccion', models.URLField(max_length=256, null=True, blank=True)),
                ('thumb', models.FilePathField(max_length=512, null=True, blank=True)),
            ],
            options={
            },
            bases=('blog.contenido',),
        ),
        migrations.CreateModel(
            name='Conferencia',
            fields=[
                ('playlist_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='contenidos.PlayList')),
            ],
            options={
            },
            bases=('contenidos.playlist',),
        ),
        migrations.CreateModel(
            name='Video',
            fields=[
                ('contenido_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='blog.Contenido')),
                ('direccion', models.URLField(max_length=256)),
            ],
            options={
            },
            bases=('blog.contenido',),
        ),
        migrations.CreateModel(
            name='Videotutorial',
            fields=[
                ('playlist_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='contenidos.PlayList')),
            ],
            options={
            },
            bases=('contenidos.playlist',),
        ),
        migrations.AddField(
            model_name='video',
            name='playlist',
            field=models.ForeignKey(related_name=b'pl_videos', to='contenidos.PlayList', null=True),
            preserve_default=True,
        ),
    ]
