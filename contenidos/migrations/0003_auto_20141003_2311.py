# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0002_auto_20141003_2311'),
        ('contenidos', '0002_auto_20141003_1625'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='conferencia',
            name='playlist_ptr',
        ),
        migrations.RemoveField(
            model_name='videotutorial',
            name='playlist_ptr',
        ),
        migrations.AlterField(
            model_name='video',
            name='playlist',
            field=models.ForeignKey(related_name=b'videos', blank=True, to='contenidos.PlayList', null=True),
        ),
        migrations.DeleteModel(
            name='Videotutorial',
        ),
        migrations.DeleteModel(
            name='Conferencia',
        ),
    ]
