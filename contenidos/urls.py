from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^admin/$', views.admin, name='admin_contenidos'),
    
    url(r'^admin/playlists/$', views.PlayListList.as_view(), name='list_playlist'),
    url(r'^admin/playlists/create/$', views.PlayListCreate.as_view(), name='create_playlist'),
    url(r'^admin/playlists/(?P<pk>\d+)/$', views.PlayListShow.as_view(), name='show_playlist'),
    url(r'^admin/playlists/(?P<pk>\d+)/edit/$', views.PlayListEdit.as_view(), name='edit_playlist'),
    url(r'^admin/playlists/(?P<pk>\d+)/delete/$', views.PlayListDelete.as_view(), name='delete_playlist'),
    
    url(r'^playlists/$', views.playlist_home, name='home_playlist'),
    url(r'^playlists/(?P<slug>\S+)/$', views.PlayListDetail.as_view(), name='detail_playlist'),
    
    url(r'^admin/videos/$', views.VideoList.as_view(), name='list_video'),
    url(r'^admin/playlists/(?P<base_id>\d+)/videos/create/$', views.VideoCreate.as_view(), name='create_video'),
    url(r'^admin/videos/(?P<pk>\d+)/$', views.VideoShow.as_view(), name='show_video'),
    url(r'^admin/playlists/(?P<base_id>\d+)/videos/(?P<pk>\d+)/edit/$', views.VideoEdit.as_view(), name='edit_video'),
    url(r'^admin/videos/(?P<pk>\d+)/delete/$', views.VideoDelete.as_view(), name='delete_video'),
    
    url(r'^videos/(?P<slug>\S+)/$', views.VideoDetail.as_view(), name='detail_video'),
    
    url(r'^admin/encuestas/$', views.EncuestaList.as_view(), name='list_encuesta'),
    url(r'^admin/encuestas/create/$', views.EncuestaCreate.as_view(), name='create_encuesta'),
    url(r'^admin/encuestas/(?P<pk>\d+)/$', views.EncuestaShow.as_view(), name='show_encuesta'),
    url(r'^admin/encuestas/(?P<pk>\d+)/edit/$', views.EncuestaEdit.as_view(), name='edit_encuesta'),
    url(r'^admin/encuestas/(?P<pk>\d+)/delete/$', views.EncuestaDelete.as_view(), name='delete_encuesta'),
    
    url(r'^encuestas/$', views.encuesta_home, name='home_encuesta'),
    url(r'^encuestas/(?P<slug>\S+)/$', views.EncuestaDetail.as_view(), name='detail_encuesta'),
    
    url(r'^admin/opciones/$', views.OpcionList.as_view(), name='list_opcion'),
    url(r'^admin/playlists/(?P<base_id>\d+)/opciones/create/$', views.OpcionCreate.as_view(), name='create_opcion'),
    url(r'^admin/opciones/(?P<pk>\d+)/$', views.OpcionShow.as_view(), name='show_opcion'),
    url(r'^admin/playlists/(?P<base_id>\d+)/opciones/(?P<pk>\d+)/edit/$', views.OpcionEdit.as_view(), name='edit_opcion'),
    url(r'^admin/opciones/(?P<pk>\d+)/delete/$', views.OpcionDelete.as_view(), name='delete_opcion'),
    
    url(r'^admin/descargas/$', views.DescargaList.as_view(), name='list_descarga'),
    url(r'^admin/descargas/create/$', views.DescargaCreate.as_view(), name='create_descarga'),
    url(r'^admin/descargas/(?P<pk>\d+)/$', views.DescargaShow.as_view(), name='show_descarga'),
    url(r'^admin/descargas/(?P<pk>\d+)/edit/$', views.DescargaEdit.as_view(), name='edit_descarga'),
    url(r'^admin/descargas/(?P<pk>\d+)/delete/$', views.DescargaDelete.as_view(), name='delete_descarga'),
]