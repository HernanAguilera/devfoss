# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('taxonomia', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Categoria',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=256)),
                ('slug', models.SlugField(max_length=256)),
                ('padre', models.ForeignKey(related_name=b'hijos', blank=True, to='taxonomia.Categoria', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
