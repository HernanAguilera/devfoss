from django.db import models

class Tag(models.Model):
    nombre = models.CharField(max_length=256)
    slug = models.SlugField(max_length=256)
    
    def __unicode__(self):
        return self.nombre
    
class Categoria(models.Model):
    nombre = models.CharField(max_length=256)
    slug = models.SlugField(max_length=256)
    
    padre = models.ForeignKey('self', related_name='hijos', null=True, blank=True)
    
    def __unicode__(self):
        return self.nombre