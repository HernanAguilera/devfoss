from django.contrib import admin
from taxonomia.models import Tag

admin.site.register(Tag)
